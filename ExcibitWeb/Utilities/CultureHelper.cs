﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace ExcibitWeb.Utilities
{
    public static class CultureHelper
    {
        private const string defaultCultureName = "ES-es";
        private static CultureInfo defaultCulture;
        public static CultureInfo DefaultCulture
        {
            get
            {
                if (defaultCulture == null)
                {
                    defaultCulture = new CultureInfo(defaultCultureName);
                }
                return defaultCulture;
            }
            private set { }
        }
        public static string DefaultCultureName
        {
            get { return defaultCultureName; }
            private set { }
        }
        public static string DefaultNavigationLanguage
        {
            get { return defaultCultureName.Substring(0, 2); }
            private set { }
        }
        public static CultureInfo CurrentCulture
        {
            get
            {
                var userCulture = HttpContext.Current.Session?["UserCulture"] ?? System.Threading.Thread.CurrentThread.CurrentUICulture.Name ?? DefaultCultureName;
                return new CultureInfo(userCulture.ToString());
            }
            set
            {
                HttpContext context = HttpContext.Current;
                if (context != null && context.Session != null)
                {
                    context.Session["UserCulture"] = value.Name;
                }
                System.Threading.Thread.CurrentThread.CurrentUICulture = value;
                System.Threading.Thread.CurrentThread.CurrentCulture = value;
            }
        }
        public static string CurrentCultureName
        {
            get { return CurrentCulture.Name; }
            private set { }
        }
        public static string CurrentNavigationLanguage
        {
            get { return CurrentCulture.Name.Substring(0, 2); }
            private set { }
        }
        public static void Initialize()
        {
            var culture = CurrentCulture;
        }
    }
}