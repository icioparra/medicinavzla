﻿using ExcibitWeb.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcibitWeb.Controllers
{
    public class HomeController : ApplicationController
    {
        public ActionResult Index()
        {
            var routeCulture = RouteData.Values["culture"]?.ToString();
            var culture = new CultureInfo(routeCulture);
            System.Threading.Thread.CurrentThread.CurrentUICulture = culture;
            System.Threading.Thread.CurrentThread.CurrentCulture = culture;
            ViewBag.OtherLang = routeCulture == "es" ? "en" : "es";
            return View();
        }
    }
}