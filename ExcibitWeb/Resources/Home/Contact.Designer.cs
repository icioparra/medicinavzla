﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExcibitWeb.Resources.Home {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Contact {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Contact() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ExcibitWeb.Resources.Home.Contact", typeof(Contact).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to and.
        /// </summary>
        public static string And {
            get {
                return ResourceManager.GetString("And", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email address.
        /// </summary>
        public static string EmailAddress {
            get {
                return ResourceManager.GetString("EmailAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Excibit Solutions Spain.
        /// </summary>
        public static string goToSpainText {
            get {
                return ResourceManager.GetString("goToSpainText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go to Excibit Solutions Spain.
        /// </summary>
        public static string goToSpainTooltip {
            get {
                return ResourceManager.GetString("goToSpainTooltip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Excibit Solutions Venezuela.
        /// </summary>
        public static string goToVzlaText {
            get {
                return ResourceManager.GetString("goToVzlaText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go to Excibit Solutions Venezuela.
        /// </summary>
        public static string goToVzlaTooltip {
            get {
                return ResourceManager.GetString("goToVzlaTooltip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Message.
        /// </summary>
        public static string Message {
            get {
                return ResourceManager.GetString("Message", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Spain.
        /// </summary>
        public static string Spain {
            get {
                return ResourceManager.GetString("Spain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit form.
        /// </summary>
        public static string SubmitForm {
            get {
                return ResourceManager.GetString("SubmitForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Talk to us.
        /// </summary>
        public static string TalkToUs {
            get {
                return ResourceManager.GetString("TalkToUs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contacting us is really easy. Feel free to express yourself using any of these &lt;button id=&quot;contact-mm-menu-toggle&quot; class=&quot;contact-mm-menu-toggle&quot;&gt;ways&lt;/button&gt;..
        /// </summary>
        public static string Text1 {
            get {
                return ResourceManager.GetString("Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Venezuela.
        /// </summary>
        public static string Venezuela {
            get {
                return ResourceManager.GetString("Venezuela", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to We are in.
        /// </summary>
        public static string WeAreIn {
            get {
                return ResourceManager.GetString("WeAreIn", resourceCulture);
            }
        }
    }
}
